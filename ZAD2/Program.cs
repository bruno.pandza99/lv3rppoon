﻿using System;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            int rows = 5;
            int columns = 5;
            MatrixGenerator matrixGenerator = MatrixGenerator.GetInstance();
            double[][] matrix = new double[rows][];
            matrix = matrixGenerator.RandMatrix(rows, columns);

            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    Console.Write(matrix[i][j].ToString() + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}
