﻿using System;
using System.Collections.Generic;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("C:\\Users\\gospodinbruno\\source\\repos\\LV3RPPOON\\ZAD1\\CSVfile.csv");
            foreach (List<string> words in data.GetData()) 
            {
                foreach (string word in words)
                {
                    Console.WriteLine(word);
                }
                Console.WriteLine();
            }
            Dataset dataClone = (Dataset)data.Clone();

            foreach (List<string> words in dataClone.GetData())
            {
                foreach (string word in words)
                {
                    Console.WriteLine(word);
                }
                Console.WriteLine();
            }
        }
    }
}
