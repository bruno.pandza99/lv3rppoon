﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD1
{
    interface Prototype
    {
        Prototype Clone();
    }
}
